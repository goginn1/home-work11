﻿namespace CloneableProject
{
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            // Создание и клонирование Car
            Car car = new Car("Toyota", 2021, 4);
            Car clonedCar = (Car)car.Clone();

            Console.WriteLine("Original Car: Manufacturer={0}, Year={1}, Doors={2}", car.Manufacturer, car.Year, car.Doors);
            Console.WriteLine("Cloned Car: Manufacturer={0}, Year={1}, Doors={2}", clonedCar.Manufacturer, clonedCar.Year, clonedCar.Doors);

            // Создание и клонирование Truck
            Truck truck = new Truck("Volvo", 2019, 5000.0f);
            Truck clonedTruck = (Truck)truck.Clone();

            Console.WriteLine("Original Truck: Manufacturer={0}, Year={1}, PayloadCapacity={2}kg", truck.Manufacturer, truck.Year, truck.PayloadCapacity);
            Console.WriteLine("Cloned Truck: Manufacturer={0}, Year={1}, PayloadCapacity={2}kg", clonedTruck.Manufacturer, clonedTruck.Year, clonedTruck.PayloadCapacity);

            // Создание и клонирование ElectricCar
            ElectricCar eCar = new ElectricCar("Tesla", 2020, 4, 100);
            ElectricCar clonedECar = (ElectricCar)eCar.Clone();

            Console.WriteLine("Original Electric Car: Manufacturer={0}, Year={1}, Doors={2}, BatteryCapacity={3}kWh", eCar.Manufacturer, eCar.Year, eCar.Doors, eCar.BatteryCapacity);
            Console.WriteLine("Cloned Electric Car: Manufacturer={0}, Year={1}, Doors={2}, BatteryCapacity={3}kWh", clonedECar.Manufacturer, clonedECar.Year, clonedECar.Doors, clonedECar.BatteryCapacity);
        }
    }
}