
using NUnit.Framework;

[TestFixture]
public class CloneTests
{
    [Test]
    public void CarCloneTest()
    {
        Car car = new Car("Ford", 2020, 4);
        Car clone = (Car)car.Clone();
        Assert.AreEqual(car.Doors, clone.Doors);
        Assert.AreEqual(car.Manufacturer, clone.Manufacturer);
        Assert.AreEqual(car.Year, clone.Year);
    }

    [Test]
    public void ElectricCarCloneTest()
    {
        ElectricCar eCar = new ElectricCar("Tesla", 2020, 4, 85);
        ElectricCar clone = (ElectricCar)eCar.Clone();
        Assert.AreEqual(eCar.BatteryCapacity, clone.BatteryCapacity);
        Assert.AreEqual(eCar.Doors, clone.Doors);
        Assert.AreEqual(eCar.Manufacturer, clone.Manufacturer);
        Assert.AreEqual(eCar.Year, clone.Year);
    }
}
