
public class Truck : Vehicle
{
    public float PayloadCapacity { get; set; }

    public Truck(string manufacturer, int year, float payloadCapacity) : base(manufacturer, year)
    {
        PayloadCapacity = payloadCapacity;
    }

    /// <summary>
    /// Клонирует текущий объект Truck.
    /// </summary>
    /// <returns>Новый объект, являющийся копией этого экземпляра.</returns>
    public override Vehicle Clone()
    {
        return new Truck(Manufacturer, Year, PayloadCapacity);
    }
}
