
using System;

public abstract class Vehicle : ICloneable, IMyCloneable<Vehicle>
{
    public string Manufacturer { get; set; }
    public int Year { get; set; }

    public Vehicle(string manufacturer, int year)
    {
        Manufacturer = manufacturer;
        Year = year;
    }

    /// <summary>
    /// Клонирует текущий объект Vehicle.
    /// </summary>
    /// <returns>Новый объект, являющийся копией этого экземпляра.</returns>
    public abstract Vehicle Clone();

    object ICloneable.Clone()
    {
        return Clone();
    }
}
