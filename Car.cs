
public class Car : Vehicle
{
    public int Doors { get; set; }

    public Car(string manufacturer, int year, int doors) : base(manufacturer, year)
    {
        Doors = doors;
    }

    /// <summary>
    /// Клонирует текущий объект Car.
    /// </summary>
    /// <returns>Новый объект, являющийся копией этого экземпляра.</returns>
    public override Vehicle Clone()
    {
        return new Car(Manufacturer, Year, Doors);
    }
}
