
public class ElectricCar : Car
{
    public int BatteryCapacity { get; set; }

    public ElectricCar(string manufacturer, int year, int doors, int batteryCapacity)
        : base(manufacturer, year, doors)
    {
        BatteryCapacity = batteryCapacity;
    }

    /// <summary>
    /// Клонирует текущий объект Electric Car.
    /// </summary>
    /// <returns>Новый объект, являющийся копией этого экземпляра.</returns>
    public override Vehicle Clone()
    {
        return new ElectricCar(Manufacturer, Year, Doors, BatteryCapacity);
    }
}
